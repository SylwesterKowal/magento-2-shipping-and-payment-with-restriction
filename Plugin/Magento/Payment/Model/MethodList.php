<?php
declare(strict_types=1);

namespace Kowal\ShippingAndPaymentWithRestriction\Plugin\Magento\Payment\Model;

class MethodList
{
    /**
     * @var \Magento\Payment\Helper\Data
     */
    protected $paymentHelper;

    /**
     * @param \Magento\Payment\Helper\Data $paymentHelper
     *
     */
    public function __construct(
        \Magento\Payment\Helper\Data $paymentHelper
    )
    {
        $this->paymentHelper = $paymentHelper;
    }

    public function aroundGetAvailableMethods(
        \Magento\Payment\Model\MethodList $subject,
        \Closure $proceed,
        \Magento\Quote\Api\Data\CartInterface $quote
    )
    {
        $paymentMethods = $proceed($quote);
        $store = $quote ? $quote->getStoreId() : null;
        $storePaymentMethods = $this->paymentHelper->getStoreMethods($store, $quote);

        $om = \Magento\Framework\App\ObjectManager::getInstance();
        $code = $om->create('Magento\Checkout\Model\Cart')->getQuote()
            ->getShippingAddress()->getShippingMethod();


        $retArray = array();
        foreach ($storePaymentMethods as $method) {
            foreach ($paymentMethods as $methodFromList) {
                if ($method->getCode() == $methodFromList->getCode()) {

                    file_put_contents('__metoda.txt', $code);
                    switch ($code) {


                        case 'pobranie_pobranie': // Kurier Pobranie
                            // pomin dla
                            if ($methodFromList->getCode() == 'pobranie' || $methodFromList->getCode() == 'cashondelivery') {
                                $retArray[] = $method;

                            }
                            break;

                        case 'odbior_odbior': // Odbiór osobisty
                            // pomin dla
                            if ($methodFromList->getCode() != 'pobranie' && $methodFromList->getCode() != 'cashondelivery') { // && $methodFromList->getCode() != 'dialcom_przelewy'
                                $retArray[] = $method;
                            }
                            break;

                        default:
                            if ($methodFromList->getCode() != 'pobranie' && $methodFromList->getCode() != 'cashondelivery' && $methodFromList->getCode() != 'gotowka') {
                                $retArray[] = $method;
                            }
                            break;
                    }

                }
            }
        }
        return $retArray;
    }
}

