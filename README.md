# Mage2 Module Kowal ShippingAndPaymentWithRestriction

    ``kowal/module-shippingandpaymentwithrestriction``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
Płatności i wysyłka z restrykcjamu

## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Kowal`
 - Enable the module by running `php bin/magento module:enable Kowal_ShippingAndPaymentWithRestriction`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require kowal/module-shippingandpaymentwithrestriction`
 - enable the module by running `php bin/magento module:enable Kowal_ShippingAndPaymentWithRestriction`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration

 - Odbior - carriers/odbior/*

 - Pobranie - carriers/pobranie/*

 - Przedplata - carriers/przedplata/*

 - Gotowka - payment/gotowka/*

 - Pobranie - payment/pobranie/*


## Specifications

 - Shipping Method
	- odbior

 - Shipping Method
	- pobranie

 - Shipping Method
	- przedplata

 - Plugin
	- aroundGetAvailableMethods - Magento\Payment\Model\MethodList > Kowal\ShippingAndPaymentWithRestriction\Plugin\Magento\Payment\Model\MethodList

 - Payment Method
	- Gotowka

 - Payment Method
	- Pobranie


## Attributes



