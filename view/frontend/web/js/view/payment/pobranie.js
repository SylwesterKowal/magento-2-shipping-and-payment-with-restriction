define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'pobranie',
                component: 'Kowal_ShippingAndPaymentWithRestriction/js/view/payment/method-renderer/pobranie-method'
            }
        );
        return Component.extend({});
    }
);